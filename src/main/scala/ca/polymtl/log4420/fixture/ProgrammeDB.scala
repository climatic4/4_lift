package ca.polymtl.log4420
package fixture

import model.{Annee, ComiteProgramme, Cheminement}

class ProgrammeDB
{
  def multimedia = ProgrammePoly.cheminementMultimedia

  val cheminements = collection.mutable.Map.empty[ Int, Cheminement ]
  val programmes = collection.mutable.Map.empty[ ComiteProgramme, List[Cheminement] ]

  def getAllProgrammes =
  {
    programmes
  }

  def get( id: Int ) =
  {
    cheminements.get( id )
  }

  def find( comite: ComiteProgramme, titre: String, annee: Annee ): Option[Cheminement] =
  {
    // on va chercher tous les cheminements qui respecte cette condition
    val res = cheminements.values.filter( cheminement =>
      ( cheminement.titre == titre ) &&
      ( cheminement.sessions match {
          case head :: rest if head.annee == annee => true
          case _ => false
        }
      )
    )

    // on s'attend à un seul cheminement pour cette ressource
    if( res.size == 1 ) Some( res.head )
    else {
      val ressourcesIdentique = res.map( _.toString ).mkString
      println( "Plusieurs cheminement pour une seule ressource " + ressourcesIdentique )

      None
    }
  }

  def add( cheminement: Cheminement )
  {
    cheminement.owner match {

      // Ajoute un nouveau programme de génie ( cheminement d'un comité de programme )

      case comite @ ComiteProgramme( _ ) => {

        val programme = programmes.getOrElse( comite, List.empty[Cheminement] )
        programmes( comite ) = cheminement :: programme
      }

      case _ => // Rien
    }

    cheminements( cheminement.hashCode() ) = cheminement
  }

  add( ProgrammePoly.cheminementMultimedia )
  add( ProgrammePoly.logicielClassique2010 )
}