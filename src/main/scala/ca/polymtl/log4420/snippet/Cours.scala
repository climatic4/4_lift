package ca.polymtl.log4420
package snippet

import net.liftweb._
import http.js.JsCmds
import http.SHtml
import util.CssSel
import util.Helpers._

import xml.NodeSeq

object Cours
{
  def apply( cours: model.Cours ): CssSel =
  {
  	".cours [id]" #> cours.sigle &
    ".sigle *" #> cours.sigle &
    ".titre *" #> cours.titre &
    ".credits *" #> cours.credit &
    ".disponibilite" #> ( "li *" #> cours.disponibilite.map( _.toString ) ) &
    ".pre-requis" #> ( "li *" #> cours.prerequis.map( _.sigle ) ) &
    ".co-requis" #> ( "li *" #> cours.corequis.map( _.sigle ) ) &
    ".tripplet .theorie *" #> cours.tripplet.theorie &
    ".tripplet .lab *" #> cours.tripplet.lab &
    ".ua .genie *" #> cours.ua.genie &
    ".ua .conception *" #> cours.ua.conception &
    ".ua .math *" #> cours.ua.math &
    ".ua .science *" #> cours.ua.science &
    ".ua .complementaire *" #> cours.ua.complementaire &
    ".sigle [onclick]" #> SHtml.ajaxInvoke( () => JsCmds.Alert( cours.titre ) )
    //"@suprimer" TODO helper pour suprimer
    //"@deplacer" TODO helper pour deplacer
    //
  }
}
