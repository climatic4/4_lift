package ca.polymtl.log4420.test

import org.scalatest.Suite
import ca.polymtl.log4420.lib.CheminementInjector
import ca.polymtl.log4420.fixture.ProgrammeDB

trait TestCheminementMock extends Suite
{ this: Suite =>

  abstract override def withFixture( test: NoArgTest )
  {
    // share nothing between tests
    CheminementInjector.DB.doWith( new ProgrammeDB() )
    {
      super.withFixture( test )
    }
  }
}
