package ca.polymtl.log4420
package test.systeme

import org.scalatest._
import org.eclipse.jetty._

import selenium.Chrome
import server.Server
import webapp.WebAppContext

import org.openqa.selenium.WebDriver


import java.util.concurrent.TimeUnit

// based on https://www.assembla.com/spaces/liftweb/wiki/Testing_With_Selenium

trait JettySpec extends FunSpec with BeforeAndAfterEach with Chrome
{
  private var server: Server = null
  private val PORT = 8081
  protected val host = "http://localhost:" + PORT + "/"

  override def beforeEach()
  {
    server = new Server( PORT )

    val context = new WebAppContext()
    context.setServer(server)
    context.setContextPath("/")
    context.setWar("src/main/webapp")

    server.setHandler(context)
    server.start()

    webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS )
  }

  override def afterEach()
  {
    webDriver.close()
    server.stop()
  }
}